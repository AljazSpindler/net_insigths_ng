﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using NetInsightsNg.Models;
using System.Linq;
using System.Threading.Tasks;

namespace NetInsightsNg.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class ProductsController : ControllerBase
  {
    private readonly AdventureWorksLT2016Context _context;
    private readonly ILogger<ProductsController> _logger;

    public ProductsController(AdventureWorksLT2016Context context, ILogger<ProductsController> logger)
    {
      _context = context;
      _logger = logger;
    }

    [HttpGet]
    public async Task<IActionResult> Index()
    {
      _logger.LogInformation("Product GET all");
      var adventureWorksLT2016Context = _context.Product.Include(p => p.ProductCategory).Include(p => p.ProductModel);
      var products = await adventureWorksLT2016Context.ToListAsync();
      return Ok(products);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> Details(int? id)
    {
      if (id == null)
      {
        return NotFound();
      }

      _logger.LogInformation("Product GET {0}", id.Value);
      var product = await _context.Product
          .Include(p => p.ProductCategory)
          .Include(p => p.ProductModel)
          .FirstOrDefaultAsync(m => m.ProductId == id);
      if (product == null)
      {
        return NotFound();
      }

      return Ok(product);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("ProductId,Name,ProductNumber,Color,StandardCost,ListPrice,Size,Weight,ProductCategoryId,ProductModelId,SellStartDate,SellEndDate,DiscontinuedDate,ThumbNailPhoto,ThumbnailPhotoFileName,Rowguid,ModifiedDate")] Product product)
    {
      if (ModelState.IsValid)
      {
        _context.Add(product);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
      }

      return BadRequest(product);
    }


    [HttpPut("{id}")]
    public async Task<IActionResult> Edit(int id, [Bind("ProductId,Name,ProductNumber,Color,StandardCost,ListPrice,Size,Weight,ProductCategoryId,ProductModelId,SellStartDate,SellEndDate,DiscontinuedDate,ThumbNailPhoto,ThumbnailPhotoFileName,Rowguid,ModifiedDate")] Product product)
    {
      if (id != product.ProductId)
      {
        return NotFound();
      }

      if (ModelState.IsValid)
      {
        try
        {
          _context.Update(product);
          await _context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
          if (!ProductExists(product.ProductId))
          {
            return NotFound();
          }
          else
          {
            throw;
          }
        }
        return RedirectToAction(nameof(Index));
      }
    
      return BadRequest(product);
    }

    [HttpDelete]
    public async Task<IActionResult> Delete(int id)
    {
      var product = await _context.Product.FindAsync(id);
      _context.Product.Remove(product);
      await _context.SaveChangesAsync();
      return RedirectToAction(nameof(Index));
    }

    private bool ProductExists(int id)
    {
      return _context.Product.Any(e => e.ProductId == id);
    }
  }
}
